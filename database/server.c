#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define PORT 4443

struct granted{
	char name[1024];
	char pass[1024];
};

struct granted_database{
	char database[1024];
	char name[1024];
};

struct table{
	int jumlahkolom;
	char type[100][1024];
	char data[100][1024];
};

void createUser(char *nama, char *pass);
int cekUserExist(char *uname );
void insertPermission(char *nama, char *database);
int cekgrantedDatabase(char *nama, char *database);
int findColumn(char *table, char *kolom);
int deleteColumn(char *table, int index);
int deleteTable(char *table, char *namaTable);
int updateColumn(char *table, int index, char *ganti);
int updateColumnWhere(char *table, int index ,char *ganti, int indexGanti ,char *where);
int deleteTableWhere(char *table, int index, char *kolom, char *where);
void writelog(char *command, char *nama);

int main(){

	int sockfd, ret;
	 struct sockaddr_in serverAddr;

	int newSocket;
	struct sockaddr_in newAddr;

	socklen_t addr_size;

	char buffer[1024];
	pid_t childpid;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd < 0){
		printf("[-]Connection Error.\n");
		exit(1);
	}
	printf("[+]Created Server Socket.\n");

	memset(&serverAddr, '\0', sizeof(serverAddr));
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(PORT);
	serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

	ret = bind(sockfd, (struct sockaddr*)&serverAddr, sizeof(serverAddr));
	if(ret < 0){
		printf("[-]Binding Error.\n");
		exit(1);
	}
	printf("[+]Bind to port %d\n", 4444);

	if(listen(sockfd, 10) == 0){
		printf("[+]Still Listening....\n");
	}else{
		printf("[-]Binding Error.\n");
	}


	while(1){
		newSocket = accept(sockfd, (struct sockaddr*)&newAddr, &addr_size);
		if(newSocket < 0){
			exit(1);
		}
		printf("Accepting Connection from %s:%d\n", inet_ntoa(newAddr.sin_addr), ntohs(newAddr.sin_port));

		if((childpid = fork()) == 0){
			close(sockfd);

			while(1){
				recv(newSocket, buffer, 1024, 0);
				char *token;
				char buffercopy[32000];
				strcpy(buffercopy, buffer);
				char command[100][1024];
				token = strtok(buffercopy, ":");
				int i=0;
				char database_used[1000];
				while( token != NULL ) {
					strcpy(command[i], token);
					i++;
					token = strtok(NULL, ":");
				}
				if(strcmp(command[0], "cUser")==0){
					if(strcmp(command[3], "0")==0){
						createUser(command[1], command[2]);
					}else{
						char warning[] = "You are not Granted Here";
						send(newSocket, warning, strlen(warning), 0);
						bzero(buffer, sizeof(buffer));
					}
				}else if(strcmp(command[0], "gPermission")==0){
					if(strcmp(command[3], "0")==0){
						int exist = cekUserExist(command[2]);
						if(exist == 1){
							insertPermission(command[2], command[1]);
						}else{
							char warning[] = "Error 404: User Not Found";
							send(newSocket, warning, strlen(warning), 0);
							bzero(buffer, sizeof(buffer));
						}
					}else{
						char warning[] = "You're Not granted";
						send(newSocket, warning, strlen(warning), 0);
						bzero(buffer, sizeof(buffer));
					}
				}else if(strcmp(command[0], "cDatabase")==0){
					char lokasi[2048];
					snprintf(lokasi, sizeof lokasi, "databases/%s", command[1]);
					printf("lokasi = %s, nama = %s , database = %s\n", lokasi, command[2], command[1]);
					mkdir(lokasi,0777);
					insertPermission(command[2], command[1]);
				}else if(strcmp(command[0], "uDatabase") == 0){
					if(strcmp(command[3], "0") != 0){
						int granted = cekgrantedDatabase(command[2], command[1]);
						if(granted != 1){
							char warning[] = "Access_database : You are Not granted Here";
							send(newSocket, warning, strlen(warning), 0);
							bzero(buffer, sizeof(buffer));
						}else{
							strncpy(database_used, command[1], sizeof(command[1]));
							char warning[] = "Access_database : granted";
							printf("database_used = %s\n", database_used);
							send(newSocket, warning, strlen(warning), 0);
							bzero(buffer, sizeof(buffer));
						}
					}
				}else if(strcmp(command[0], "cekCurrentDatabase")==0){
					if(database_used[0] == '\0'){
						strcpy(database_used, "Database not selected");
					}
					send(newSocket, database_used, strlen(database_used), 0);
					bzero(buffer, sizeof(buffer));
				}else if(strcmp(command[0], "cTable")==0){
					printf("%s\n", command[1]);
					char *tokens;
					if(database_used[0] == '\0'){
						strcpy(database_used, "Database not selected");
						send(newSocket, database_used, strlen(database_used), 0);
						bzero(buffer, sizeof(buffer));
					}else{
                        char daftarQuery[100][1024];
                        char copycommand[2048];
                        snprintf(copycommand, sizeof copycommand, "%s", command[1]);
                        tokens = strtok(copycommand, "(), ");
                        int jumlah=0;
                        while( tokens != NULL ) {
                            strcpy(daftarQuery[jumlah], tokens);
                            printf("%s\n", daftarQuery[jumlah]);
                            jumlah++;
                            tokens = strtok(NULL, "(), ");
                        }
                        char buatTable[2048];
                        snprintf(buatTable, sizeof buatTable, "../database/databases/%s/%s", database_used, daftarQuery[2]);
                        int iterasi = 0;
                        int iterasiData = 3;
                        struct table kolom;
                        while(jumlah > 3){
                            strcpy(kolom.data[iterasi], daftarQuery[iterasiData]);
                            printf("%s\n", kolom.data[iterasi]);
                            strcpy(kolom.type[iterasi], daftarQuery[iterasiData+1]);
                            iterasiData = iterasiData+2;
                            jumlah=jumlah-2;
                            iterasi++;
                        }
                        kolom.jumlahkolom = iterasi;
                        printf("iterasi = %d\n", iterasi);
                        FILE *fptr;
                        printf("%s\n", buatTable);
                        fptr=fopen(buatTable,"ab");
                        fwrite(&kolom,sizeof(kolom),1,fptr);
                        fclose(fptr);
                    }
				}else if(strcmp(command[0], "dDatabase")==0){
					int granted = cekgrantedDatabase(command[2], command[1]);
					if(granted != 1){
						char warning[] = "Access_database : You are Not granted Here";
						send(newSocket, warning, strlen(warning), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}else{
						char hapus[2048];
						snprintf(hapus, sizeof hapus, "rm -r databases/%s", command[1]);
						system(hapus);
						char warning[] = "Database Successfully Removed";
						send(newSocket, warning, strlen(warning), 0);
						bzero(buffer, sizeof(buffer));
					}
				}else if(strcmp(command[0], "dTable")==0){
					if(database_used[0] == '\0'){
						strcpy(database_used, "Database not selected");
						send(newSocket, database_used, strlen(database_used), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
					char hapus[2048];
					snprintf(hapus, sizeof hapus, "databases/%s/%s", database_used ,command[1]);
					remove(hapus);
					char warning[] = "Table Successfully Removed";
					send(newSocket, warning, strlen(warning), 0);
					bzero(buffer, sizeof(buffer));
				}else if(strcmp(command[0], "dColumn")==0){
					if(database_used[0] == '\0'){
						strcpy(database_used, "Database not selected");
						send(newSocket, database_used, strlen(database_used), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
					char buatTable[2048];
					snprintf(buatTable, sizeof buatTable, "databases/%s/%s", database_used, command[2]);
					int index = findColumn(buatTable, command[1]);
                    if(index == -1){
                        char warning[] = "Column Not Found";
                        send(newSocket, warning, strlen(warning), 0);
                        bzero(buffer, sizeof(buffer));
                        continue;
                    }
					deleteColumn(buatTable, index);
					char warning[] = "Column Has Been Removed";
					send(newSocket, warning, strlen(warning), 0);
					bzero(buffer, sizeof(buffer));
				}else if(strcmp(command[0], "insert")==0){
                    if(database_used[0] == '\0'){
						strcpy(database_used, "You're not selecting database yet");
						send(newSocket, database_used, strlen(database_used), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
                    char daftarQuery[100][1024];
					char copycommand[2048];
					snprintf(copycommand, sizeof copycommand, "%s", command[1]);
                    char *tokens;
                    tokens = strtok(copycommand, "\'(), ");
					int jumlah=0;
					while( tokens != NULL ) {
						strcpy(daftarQuery[jumlah], tokens);
						jumlah++;
						tokens = strtok(NULL, "\'(), ");
					}
                    char buatTable[2048];
					snprintf(buatTable, sizeof buatTable, "databases/%s/%s", database_used, daftarQuery[2]);
                    FILE *fptr;
                    int banyakKolom;
					fptr=fopen(buatTable,"r");
                    if (fptr == NULL){
                        char warning[] = "TABLE NOT FOUND";
                        send(newSocket, warning, strlen(warning), 0);
                        bzero(buffer, sizeof(buffer));
                        continue;
                    }else{
                        struct table user;
                        fread(&user,sizeof(user),1,fptr);
                        banyakKolom=user.jumlahkolom;
                        fclose(fptr);
                    }
					int iterasi = 0;
					int iterasiData = 3;
					struct table kolom;
					while(jumlah > 3){
						strcpy(kolom.data[iterasi], daftarQuery[iterasiData]);
                        printf("%s\n", kolom.data[iterasi]);
						strcpy(kolom.type[iterasi], "string");
						iterasiData++;
						jumlah=jumlah-1;
						iterasi++;
					}
					kolom.jumlahkolom = iterasi;
                    if(banyakKolom != kolom.jumlahkolom){
                        char warning[] = "YOUR INPUT NOT MATCH THE COLUMN";
                        send(newSocket, warning, strlen(warning), 0);
                        bzero(buffer, sizeof(buffer));
                        continue;
                    }   
					printf("iterasi = %d\n", iterasi);
                    FILE *fptr1;
                    printf("%s\n", buatTable);
                    fptr1=fopen(buatTable,"ab");
                    fwrite(&kolom,sizeof(kolom),1,fptr1);
                    fclose(fptr1);
                    char warning[] = "Data Has Been Inserted";
					send(newSocket, warning, strlen(warning), 0);
					bzero(buffer, sizeof(buffer));
                }else if(strcmp(command[0], "update")==0){
                    if(database_used[0] == '\0'){
						strcpy(database_used, "You're not selecting database yet");
						send(newSocket, database_used, strlen(database_used), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
                    char daftarQuery[100][1024];
					char copycommand[2048];
					snprintf(copycommand, sizeof copycommand, "%s", command[1]);
                    char *tokens;
                    tokens = strtok(copycommand, "\'(),= ");
					int jumlah=0;
					while( tokens != NULL ) {
						strcpy(daftarQuery[jumlah], tokens);
						printf("%s\n", daftarQuery[jumlah]);
						jumlah++;
						tokens = strtok(NULL, "\'(),= ");
					}
                    printf("jumlah = %d\n", jumlah);
                    char buatTable[2048];
					snprintf(buatTable, sizeof buatTable, "databases/%s/%s", database_used, daftarQuery[1]);
                    if(jumlah==5){
                        printf("buat table = %s, kolumn = %s", buatTable, daftarQuery[3]);
                        int index = findColumn(buatTable, daftarQuery[3]);
                        if(index == -1){
                            char warning[] = "Column Not Found";
                            send(newSocket, warning, strlen(warning), 0);
                            bzero(buffer, sizeof(buffer));
                            continue;
                        }
                        printf("index = %d\n", index);
                        updateColumn(buatTable, index, daftarQuery[4]);
                    }else if(jumlah==8){
                        printf("buat table = %s, kolumn = %s", buatTable, daftarQuery[3]);
                        int index = findColumn(buatTable, daftarQuery[3]);
                        if(index == -1){
                            char warning[] = "Column Not Found";
                            send(newSocket, warning, strlen(warning), 0);
                            bzero(buffer, sizeof(buffer));
                            continue;
                        }
                        printf("%s\n", daftarQuery[7]);
                        int indexGanti = findColumn(buatTable, daftarQuery[6]);
                        updateColumnWhere(buatTable, index, daftarQuery[4], indexGanti ,daftarQuery[7]);
                    }else{
                        char warning[] = "Data Has Been Deleted";
                        send(newSocket, warning, strlen(warning), 0);
                        bzero(buffer, sizeof(buffer));
                        continue;
                    }
                    char warning[] = "Data Has Been Updated";
					send(newSocket, warning, strlen(warning), 0);
					bzero(buffer, sizeof(buffer));

                }else if(strcmp(command[0], "delete")==0){
                    if(database_used[0] == '\0'){
						strcpy(database_used, "You're not selecting database yet");
						send(newSocket, database_used, strlen(database_used), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
                    char daftarQuery[100][1024];
					char copycommand[2048];
					snprintf(copycommand, sizeof copycommand, "%s", command[1]);
                    char *tokens;
                    tokens = strtok(copycommand, "\'(),= ");
					int jumlah=0;
					while( tokens != NULL ) {
						strcpy(daftarQuery[jumlah], tokens);
						printf("%s\n", daftarQuery[jumlah]);
						jumlah++;
						tokens = strtok(NULL, "\'(),= ");
					}
                    printf("jumlah = %d\n", jumlah);
                    char buatTable[2048];
					snprintf(buatTable, sizeof buatTable, "databases/%s/%s", database_used, daftarQuery[2]);
                    if(jumlah==3){
                        deleteTable(buatTable, daftarQuery[2]);
                    }else if(jumlah==6){
                        int index = findColumn(buatTable, daftarQuery[4]);
                        if(index == -1){
                            char warning[] = "Column Not Found";
                            send(newSocket, warning, strlen(warning), 0);
                            bzero(buffer, sizeof(buffer));
                            continue;
                        }
                        printf("index  = %d\n", index);
                        deleteTableWhere(buatTable, index, daftarQuery[4], daftarQuery[5]);
                    }else{
                        char warning[] = "Input Salah";
                        send(newSocket, warning, strlen(warning), 0);
                        bzero(buffer, sizeof(buffer));
                        continue;
                    }
                    char warning[] = "Data Has Been Deleted";
					send(newSocket, warning, strlen(warning), 0);
					bzero(buffer, sizeof(buffer));
                }else if(strcmp(command[0], "select")==0){
                    if(database_used[0] == '\0'){
						strcpy(database_used, "You're not selecting database yet");
						send(newSocket, database_used, strlen(database_used), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
                    char daftarQuery[100][1024];
					char copycommand[2048];
					snprintf(copycommand, sizeof copycommand, "%s", command[1]);
                    char *tokens;
                    tokens = strtok(copycommand, "\'(),= ");
					int jumlah=0;
					while( tokens != NULL ) {
						strcpy(daftarQuery[jumlah], tokens);
						printf("%s\n", daftarQuery[jumlah]);
						jumlah++;
						tokens = strtok(NULL, "\'(),= ");
					}
					printf("ABC\n");
                    if(jumlah == 4){
						char buatTable[2048];
						snprintf(buatTable, sizeof buatTable, "databases/%s/%s", database_used, daftarQuery[3]);
						printf("buat table = %s", buatTable);
                        char commandKolom[1000];
                        printf("masuk 4\n");
                        if(strcmp(daftarQuery[1], "*")==0){
                            FILE *fptr, *fptr1;
                            struct table user;
                            int id,found=0;
                            fptr=fopen(buatTable,"rb");
                            char buffers[40000];
                            char sendDatabase[40000];
                            bzero(buffer, sizeof(buffer));
                            bzero(sendDatabase, sizeof(sendDatabase));
                            while(1){	
                                char enter[] = "\n";
                                fread(&user,sizeof(user),1,fptr);
                                snprintf(buffers, sizeof buffers, "\n");
                                if(feof(fptr)){
                                    break;
                                }
                                for(int i=0; i< user.jumlahkolom; i++){
                                    char padding[2048];
                                    snprintf(padding, sizeof padding, "%s\t",user.data[i]);
                                    strcat(buffers, padding);
                                }
                                strcat(sendDatabase, buffers);
                            }
                            send(newSocket, sendDatabase, strlen(sendDatabase), 0);
                            bzero(sendDatabase, sizeof(sendDatabase));
                            bzero(buffer, sizeof(buffer));
                            fclose(fptr);
                        }else{
                            int index = findColumn(buatTable, daftarQuery[1]);
                            printf("%d\n", index);
                            FILE *fptr, *fptr1;
                            struct table user;
                            int id,found=0;
                            fptr=fopen(buatTable,"rb");
                            char buffers[40000];
                            char sendDatabase[40000];
                            bzero(buffer, sizeof(buffer));
                            bzero(sendDatabase, sizeof(sendDatabase));
                            while(1){	
                                char enter[] = "\n";
                                fread(&user,sizeof(user),1,fptr);
                                snprintf(buffers, sizeof buffers, "\n");
                                if(feof(fptr)){
                                    break;
                                }
                                for(int i=0; i< user.jumlahkolom; i++){
                                    if(i == index){
                                        char padding[2048];
                                        snprintf(padding, sizeof padding, "%s\t",user.data[i]);
                                        strcat(buffers, padding);
                                    }
                                }
                                strcat(sendDatabase, buffers);
                            }
                            printf("ini send fix\n%s\n", sendDatabase);
                            fclose(fptr);
                            send(newSocket, sendDatabase, strlen(sendDatabase), 0);
                            bzero(sendDatabase, sizeof(sendDatabase));
                            bzero(buffer, sizeof(buffer));
                        }
                    }else if(jumlah == 7 && strcmp(daftarQuery[4], "WHERE")==0){
						char buatTable[2048];
						snprintf(buatTable, sizeof buatTable, "databases/%s/%s", database_used, daftarQuery[3]);
						printf("buat table = %s", buatTable);
                        char commandKolom[1000];
                        printf("masuk 4\n");
                        if(strcmp(daftarQuery[1], "*")==0){
                            FILE *fptr, *fptr1;
                            struct table user;
                            int id,found=0;
                            fptr=fopen(buatTable,"rb");
                            char buffers[40000];
                            char sendDatabase[40000];
                            int index = findColumn(buatTable, daftarQuery[5]);
                            printf("%d\n", index);
                            bzero(buffer, sizeof(buffer));
                            bzero(sendDatabase, sizeof(sendDatabase));
                            while(1){	
                                char enter[] = "\n";
                                fread(&user,sizeof(user),1,fptr);
                                snprintf(buffers, sizeof buffers, "\n");
                                if(feof(fptr)){
                                    break;
                                }
                                for(int i=0; i< user.jumlahkolom; i++){
                                    if(strcmp(user.data[index], daftarQuery[6])==0){
                                        char padding[2048];
                                        snprintf(padding, sizeof padding, "%s\t",user.data[i]);
                                        strcat(buffers, padding);
                                    }
                                    
                                }
                               
                                strcat(sendDatabase, buffers);
                            }
                            
                            send(newSocket, sendDatabase, strlen(sendDatabase), 0);
                            bzero(sendDatabase, sizeof(sendDatabase));
                            bzero(buffer, sizeof(buffer));
                            fclose(fptr);
                        }else{
                            
                            int index = findColumn(buatTable, daftarQuery[1]);
                            printf("%d\n", index);
                            FILE *fptr, *fptr1;
                            struct table user;
                            int id,found=0;
                            int indexGanti = findColumn(buatTable, daftarQuery[5]);
                            fptr=fopen(buatTable,"rb");
                            char buffers[40000];
                            char sendDatabase[40000];
                            bzero(buffer, sizeof(buffer));
                            bzero(sendDatabase, sizeof(sendDatabase));
                            while(1){	
                                char enter[] = "\n";
                                
                                fread(&user,sizeof(user),1,fptr);
                                snprintf(buffers, sizeof buffers, "\n");
                                // send(newSocket, enter, strlen(enter), 0);
                                if(feof(fptr)){
                                    break;
                                }
                                for(int i=0; i< user.jumlahkolom; i++){
                                    if(i == index && (strcmp(user.data[indexGanti], daftarQuery[6])==0 || strcmp(user.data[i],daftarQuery[5])==0)){
                                        char padding[2048];
                                        snprintf(padding, sizeof padding, "%s\t",user.data[i]);
                                        strcat(buffers, padding);
                                    }
                                 
                                }
                                
                                strcat(sendDatabase, buffers);
                            }
                            printf("ini send fix\n%s\n", sendDatabase);
                            fclose(fptr);
                            send(newSocket, sendDatabase, strlen(sendDatabase), 0);
                            bzero(sendDatabase, sizeof(sendDatabase));
                            bzero(buffer, sizeof(buffer));
                        }
                    }else{
						printf("ini query 3 %s", daftarQuery[jumlah-3]);
						if(strcmp(daftarQuery[jumlah-3], "WHERE")!= 0){
							char buatTable[2048];
							snprintf(buatTable, sizeof buatTable, "databases/%s/%s", database_used, daftarQuery[jumlah-1]);
							printf("buat table = %s", buatTable);
							printf("tanpa where");
							int index[100];
							int iterasi=0;
							for(int i=1; i<jumlah-2; i++){
								index[iterasi] = findColumn(buatTable, daftarQuery[i]);
								printf("%d\n", index[iterasi]);
								iterasi++;
							}
						}else if(strcmp(daftarQuery[jumlah-3], "WHERE")== 0){
							printf("dengan where");
						}
					}
                }else if(strcmp(command[0], "log")==0){
					writelog(command[1], command[2]);
					char warning[] = "\n";
					send(newSocket, warning, strlen(warning), 0);
					bzero(buffer, sizeof(buffer));
				}
				if(strcmp(buffer, ":exit") == 0){
					printf("Disconnected from %s:%d\n", inet_ntoa(newAddr.sin_addr), ntohs(newAddr.sin_port));
					break;
				}else{
					printf("Client: %s\n", buffer);
					send(newSocket, buffer, strlen(buffer), 0);
					bzero(buffer, sizeof(buffer));
				}
			}
		}

	}

	close(newSocket);


	return 0;
}

void createUser(char *nama, char *pass){
	struct granted user;
	strcpy(user.name, nama);
	strcpy(user.pass, pass);
	printf("%s %s\n", user.name, user.pass);
	char fname[]={"databases/user.dat"};
	FILE *fptr;
	fptr=fopen(fname,"ab");
	fwrite(&user,sizeof(user),1,fptr);
	fclose(fptr);
}

int cekUserExist(char *uname ){
	FILE *fptr;
	struct granted user;
	int id,found=0;
	fptr=fopen("../database/databases/user.dat","rb");
	while(1){	
		fread(&user,sizeof(user),1,fptr);
		
		if(strcmp(user.name, uname)==0){
			return 1;
		}
		if(feof(fptr)){
			break;
		}
	}
	fclose(fptr);
	return 0;
	
}

void insertPermission(char *nama, char *database){
	struct granted_database user;
	strcpy(user.name, nama);
	strcpy(user.database, database);
	printf("%s %s\n", user.name, user.database);
	char fname[]={"databases/permission.dat"};
	FILE *fptr;
	fptr=fopen(fname,"ab");
	fwrite(&user,sizeof(user),1,fptr);
	fclose(fptr);
}

int cekgrantedDatabase(char *nama, char *database){
	FILE *fptr;
	struct granted_database user;
	int id,found=0;
	printf("nama = %s  database = %s", nama, database);
	fptr=fopen("../database/databases/permission.dat","rb");
	while(1){	
		fread(&user,sizeof(user),1,fptr);
		if(strcmp(user.name, nama)==0){
			if(strcmp(user.database, database)==0){
				return 1;
			}
		}
		if(feof(fptr)){
			break;
		}
	}
	fclose(fptr);
	return 0;
}

int findColumn(char *table, char *kolom){
	FILE *fptr;
	struct table user;
	int id,found=0;
	fptr=fopen(table,"rb");	
	fread(&user,sizeof(user),1,fptr);
	int index=-1;
	for(int i=0; i < user.jumlahkolom; i++){
		if(strcmp(user.data[i], kolom)==0){
			index = i;
		}
	}
    if(feof(fptr)){
		return -1;
	}
	fclose(fptr);
	return index;
}

int deleteColumn(char *table, int index){
	FILE *fptr, *fptr1;
	struct table user;
	int id,found=0;
	fptr=fopen(table,"rb");
	fptr1=fopen("temp","wb");
	while(1){	
		fread(&user,sizeof(user),1,fptr);
        if(feof(fptr)){
			break;
		}
		struct table userCopy;
		int iterasi=0;
		for(int i=0; i< user.jumlahkolom; i++){
			if(i == index){
				continue;
			}
			strcpy(userCopy.data[iterasi], user.data[i]);
			strcpy(userCopy.type[iterasi], user.type[i]);
			printf("%s\n", userCopy.data[iterasi]);
			iterasi++;
		}
		userCopy.jumlahkolom = user.jumlahkolom-1;
		fwrite(&userCopy,sizeof(userCopy),1,fptr1);
	}
	fclose(fptr);
	fclose(fptr1);
	remove(table);
	rename("temp", table);
	return 0;
}

int deleteTable(char *table, char *namaTable){
    FILE *fptr, *fptr1;
	struct table user;
	int id,found=0;
	fptr=fopen(table,"rb");
    fptr1=fopen("temp","ab");	
	fread(&user,sizeof(user),1,fptr);
	int index=-1;
    struct table userCopy;
	for(int i=0; i < user.jumlahkolom; i++){
        strcpy(userCopy.data[i], user.data[i]);
        strcpy(userCopy.type[i], user.type[i]);
	}
    userCopy.jumlahkolom = user.jumlahkolom;
    fwrite(&userCopy,sizeof(userCopy),1,fptr1);
    fclose(fptr);
	fclose(fptr1);
    remove(table);
	rename("temp", table);
	return 1;
}

int updateColumn(char *table, int index, char *ganti){
    FILE *fptr, *fptr1;
	struct table user;
	int id,found=0;
	fptr=fopen(table,"rb");
    fptr1=fopen("temp","ab");
    int datake = 0;
	while(1){	
		fread(&user,sizeof(user),1,fptr);
        if(feof(fptr)){
			break;
		}
        struct table userCopy;
        int iterasi=0;
        for(int i=0; i< user.jumlahkolom; i++){
            if(i == index && datake!=0){
                strcpy(userCopy.data[iterasi], ganti);
            }else{
                strcpy(userCopy.data[iterasi], user.data[i]);
            }
            printf("%s\n", userCopy.data[iterasi]);
            strcpy(userCopy.type[iterasi], user.type[i]);
            printf("%s\n", userCopy.data[iterasi]);
            iterasi++;
        }
        userCopy.jumlahkolom = user.jumlahkolom;
        fwrite(&userCopy,sizeof(userCopy),1,fptr1);
        datake++;
	}
	fclose(fptr);
	fclose(fptr1);
	remove(table);
	rename("temp", table);
	return 0;
}

int updateColumnWhere(char *table, int index ,char *ganti, int indexGanti ,char *where){
    FILE *fptr, *fptr1;
	struct table user;
	int id,found=0;
	fptr=fopen(table,"rb");
    fptr1=fopen("temp","ab");
    int datake = 0;
	while(1){	
		fread(&user,sizeof(user),1,fptr);
        if(feof(fptr)){
			break;
		}
        struct table userCopy;
        int iterasi=0;
        for(int i=0; i< user.jumlahkolom; i++){
            if(i == index && datake!=0 && strcmp(user.data[indexGanti], where)==0){
                strcpy(userCopy.data[iterasi], ganti);
            }else{
                strcpy(userCopy.data[iterasi], user.data[i]);
            }
            printf("%s\n", userCopy.data[iterasi]);
            strcpy(userCopy.type[iterasi], user.type[i]);
            printf("%s\n", userCopy.data[iterasi]);
            iterasi++;
        }
        userCopy.jumlahkolom = user.jumlahkolom;
        fwrite(&userCopy,sizeof(userCopy),1,fptr1);
        datake++;
	}
	fclose(fptr);
	fclose(fptr1);
	remove(table);
	rename("temp", table);
	return 0;
}

int deleteTableWhere(char *table, int index, char *kolom, char *where){
    FILE *fptr, *fptr1;
	struct table user;
	int id,found=0;
	fptr=fopen(table,"rb");
    fptr1=fopen("temp","ab");
    int datake = 0;
	while(1){	
        found = 0;
		fread(&user,sizeof(user),1,fptr);
        if(feof(fptr)){
			break;
		}
        struct table userCopy;
        int iterasi=0;
        for(int i=0; i< user.jumlahkolom; i++){
            if(i == index && datake!=0 && strcmp(user.data[i], where)==0){
                found = 1;
            }
            strcpy(userCopy.data[iterasi], user.data[i]);
            printf("%s\n", userCopy.data[iterasi]);
            strcpy(userCopy.type[iterasi], user.type[i]);
            printf("%s\n", userCopy.data[iterasi]);
            iterasi++;
        }
        userCopy.jumlahkolom = user.jumlahkolom;
        if(found != 1){
            fwrite(&userCopy,sizeof(userCopy),1,fptr1);
        }
        datake++;
	}
	fclose(fptr);
	fclose(fptr1);
	remove(table);
	rename("temp", table);
	return 0;
}

void writelog(char *command, char *nama){
	time_t rawtime;
	struct tm *timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);
 
	char infoWriteLog[1000];

	FILE *file;
	file = fopen("logUser.log", "ab");

	sprintf(infoWriteLog, "%d-%.2d-%.2d %.2d:%.2d:%.2d::%s::%s\n",timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, nama, command);
	fputs(infoWriteLog, file);
	fclose(file);
	return;
}