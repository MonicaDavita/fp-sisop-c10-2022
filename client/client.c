#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <ctype.h>
#include <time.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define PORT 4443

struct granted{
	char name[1024];
	char password[1024];
};

int cekgranted(char *username, char *password);
void writelog(char *command, char *nama);

int main(int argc, char *argv[]){
	int granted=0;
	int id_user = geteuid();
	char database_used[1000];
	if(geteuid() == 0){
		granted=1;
	}else{
		int id = geteuid();
		granted = cekgranted(argv[2],argv[4]);
	}
	if(granted==0){
		return 0;
	}
	int clientSocket, ret;
	struct sockaddr_in serverAddr;
	char buffer[2048];

	clientSocket = socket(AF_INET, SOCK_STREAM, 0);
	if(clientSocket < 0){
		printf("[-]Error in connection.\n");
		exit(1);
	}
	printf("[+]Successfully created client socket.\n");

	memset(&serverAddr, '\0', sizeof(serverAddr));
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(PORT);
	serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

	ret = connect(clientSocket, (struct sockaddr*)&serverAddr, sizeof(serverAddr));
	if(ret < 0){
		printf("[-]Connection Error.\n");
		exit(1);
	}
	printf("[+]Server is Connected.\n");
	while(1){
		printf("Client: \t");
		char input[1024];
		char copyinput[1024];
		char command[100][1024];
		char *token;
		int i=0;  
		scanf(" %[^\n]s", input);
		strcpy(copyinput, input);
		token = strtok(input, " ");
		while( token != NULL ) {
			strcpy(command[i], token);
			i++;
			token = strtok(NULL, " ");
		}
		int wrongCommand = 0;
		if(strcmp(command[0], "CREATE")==0){
			if(strcmp(command[1], "USER")==0 && strcmp(command[3], "IDENTIFIED")==0 && strcmp(command[4], "BY")==0){
				snprintf(buffer, sizeof buffer, "cUser:%s:%s:%d", command[2], command[5], id_user);
				send(clientSocket, buffer, strlen(buffer), 0);
			}else if(strcmp(command[1], "DATABASE")==0){
				snprintf(buffer, sizeof buffer, "cDatabase:%s:%s:%d", command[2], argv[2], id_user);
				send(clientSocket, buffer, strlen(buffer), 0);
			}else if(strcmp(command[1], "TABLE")==0){
				snprintf(buffer, sizeof buffer, "cTable:%s", copyinput);
				send(clientSocket, buffer, strlen(buffer), 0);
			}
		}else if(strcmp(command[0], "GRANT")==0 && strcmp(command[1], "PERMISSION")==0 && strcmp(command[3], "INTO")==0){
			snprintf(buffer, sizeof buffer, "gPermission:%s:%s:%d", command[2],command[4], id_user);
			send(clientSocket, buffer, strlen(buffer), 0);
		}else if(strcmp(command[0], "USE")==0){
			snprintf(buffer, sizeof buffer, "uDatabase:%s:%s:%d", command[1], argv[2], id_user);
			send(clientSocket, buffer, strlen(buffer), 0);
		}else if(strcmp(command[0], "cekCurrentDatabase")==0){
			snprintf(buffer, sizeof buffer, "%s", command[0]);
			send(clientSocket, buffer, strlen(buffer), 0);
		}else if(strcmp(command[0], "DROP")==0){
			if(strcmp(command[1], "DATABASE")==0){
				snprintf(buffer, sizeof buffer, "dDatabase:%s:%s", command[2], argv[2]);
				send(clientSocket, buffer, strlen(buffer), 0);
			}else if(strcmp(command[1], "TABLE")==0){
				snprintf(buffer, sizeof buffer, "dTable:%s:%s", command[2], argv[2]);
				send(clientSocket, buffer, strlen(buffer), 0);
			}else if(strcmp(command[1], "COLUMN")==0){
				snprintf(buffer, sizeof buffer, "dColumn:%s:%s:%s", command[2], command[4] ,argv[2]);
				send(clientSocket, buffer, strlen(buffer), 0);
			}
		}else if(strcmp(command[0], "INSERT")==0 && strcmp(command[1], "INTO")==0){
            snprintf(buffer, sizeof buffer, "insert:%s", copyinput);
			send(clientSocket, buffer, strlen(buffer), 0);
        }else if(strcmp(command[0], "UPDATE")==0){
            snprintf(buffer, sizeof buffer, "update:%s", copyinput);
			send(clientSocket, buffer, strlen(buffer), 0);
        }else if(strcmp(command[0], "DELETE")==0){
            snprintf(buffer, sizeof buffer, "delete:%s", copyinput);
            send(clientSocket, buffer, strlen(buffer), 0);
        }else if(strcmp(command[0], "SELECT")==0){
            snprintf(buffer, sizeof buffer, "select:%s", copyinput);
            send(clientSocket, buffer, strlen(buffer), 0);
        }else if(strcmp(command[0], ":exit")!=0){
			wrongCommand = 1;
			char peringatan[] = "Invalid Command";
			send(clientSocket, peringatan, strlen(peringatan), 0);
		}

		if(wrongCommand != 1){
			char namaSender[1024];
			if(id_user == 0){
				strcpy(namaSender, "root");
			}else{
				strcpy(namaSender, argv[2]);
			}
			writelog(copyinput, namaSender);
		}

		if(strcmp(command[0], ":exit") == 0){
			send(clientSocket, command[0], strlen(command[0]), 0);
			close(clientSocket);
			printf("[-]Server is Disconnected.\n");
			exit(1);
		}
		bzero(buffer, sizeof(buffer));
		if(recv(clientSocket, buffer, 1024, 0) < 0){
			printf("[-]Error in getting data.\n");
		}else{
			printf("Server: \t%s\n", buffer);
		}
	}

	return 0;
}

int cekgranted(char *username, char *password){
	FILE *fptr;
	struct granted user;
	int id,found=0;
	fptr=fopen("../database/databases/user.dat","rb");
	while(1){	
		fread(&user,sizeof(user),1,fptr);

		if(strcmp(user.name, username)==0){
			if(strcmp(user.password, password)==0){
				found=1;
			}
		}
		if(feof(fptr)){
			break;
		}
	}
	fclose(fptr);
	if(found==0){
		printf("Ungranted\n");
		return 0;
	}else{
		return 1;
	}
	
}

void writelog(char *command, char *nama){
	time_t rawtime;
	struct tm *timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);
 
	char infoWriteLog[1000];

	FILE *file;
	char lokasi[1024];
	snprintf(lokasi, sizeof lokasi, "../database/log/log%s.log", nama);
	file = fopen(lokasi, "ab");

	sprintf(infoWriteLog, "%d-%.2d-%.2d %.2d:%.2d:%.2d:%s:%s;\n",timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, nama, command);

	fputs(infoWriteLog, file);
	fclose(file);
	return;
}